package com.example.fireport.ui;

import com.example.fireport.handler.FireportHandler;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends VerticalLayout {

    public MainView(FireportHandler fireportHandler) {
        setPadding(false);
        setHeight("100%");
        setMargin(false);
        setPadding(false);
        setSpacing(false);
        getStyle().set("background-color", "#4D4D4D");
        add(getHeader());
        add(new IFrame("http://192.168.31.18:5601/app/kibana#/visualize/edit/9bedff30-d478-11e8-b494-55504827fc77?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-90d%2Cmode%3Aquick%2Cto%3Anow))", "600px", "100%"));
        add(addControlElements(fireportHandler));
    }

    private Component getHeader() {
        Div header = new Div();
        header.getStyle().set("background-color", "#333333");
        header.setWidth("100%");
        header.getStyle().set("padding", "10px");
        Image fireport = new Image("/logo_long.png", "Fireport");
        fireport.setHeight("40px");
        header.add(fireport);
        return header;
    }

    private Component addControlElements(FireportHandler fireportHandler) {
        HorizontalLayout elements = new HorizontalLayout();
        elements.setWidth("100%");
        elements.add(new ReportPanel(fireportHandler));
        elements.add(new SubscribePanel(fireportHandler));
        return elements;
    }

}
