package com.example.fireport.ui;

import com.example.fireport.handler.FireportHandler;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Input;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class ReportPanel extends VerticalLayout {

    private HorizontalLayout latLon;
    private OptionalUpload upload;
    private Button reportB;
    private Button sendB;
    private Label label = new Label("Done!");
    private boolean visible;

    public ReportPanel(FireportHandler fireportHandler) {
        setWidth("100%");

        reportB = new Button("Report fire", e -> {
            visible = !visible;
            latLon.setVisible(visible);
            upload.setVisible(visible);
            sendB.setVisible(visible);
            remove(label);
        });
        reportB.getStyle().set("background-color", "#FF9900");
        reportB.getStyle().set("color", "#FFDf00");

        latLon = new HorizontalLayout();
        latLon.setVisible(visible);

        Input lat = new Input();
        setStyleGrey(lat);
        lat.setPlaceholder("Latitude");
        latLon.add(lat);

        Input lon = new Input();
        setStyleGrey(lon);
        lon.setPlaceholder("Longitude");
        latLon.add(lon);

        upload = new OptionalUpload(fireportHandler);
        setStyleGrey(upload);
        upload.setVisible(visible);

        sendB = new Button("Send", e -> {
            fireportHandler.process(upload.getFilename(), lat.getValue(), lon.getValue());
            add(label);
        });
        setStyleGrey(sendB);
        sendB.setVisible(visible);

        add(reportB);
        add(latLon);
        add(upload);
        add(sendB);
    }

    private void setStyleGrey(HasStyle component) {
        component.getStyle().set("background-color", "#666666");
        component.getStyle().set("color", "#999999");
        component.getStyle().set("outline", "none");
        component.getStyle().set("box-shadow", "none");
        component.getStyle().set("border", "none");
    }

}
