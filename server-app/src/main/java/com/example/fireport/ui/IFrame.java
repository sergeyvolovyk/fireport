package com.example.fireport.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;

@Tag("iframe")
public class IFrame extends Component {

    public IFrame(String src, String height, String width) {
        getElement().setProperty("src", src);
        getElement().setProperty("height", height);
        getElement().setProperty("width", width);
    }

}
