package com.example.fireport.handler;

import com.example.fireport.entity.FireReport;
import com.example.fireport.entity.LatLon;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class FireportHandler {

    private final ElasticFacade elasticFacade;
    private final AlarmMailService alarmMailService;
    private Map<String, LatLon> emails = new HashMap<>();

    public void process(String filename, String lat, String lon) {
        System.out.println("report: filename=" + filename + " lat=" + lat + " lon=" + lon);
        FireReport fireReport = new FireReport();
        fireReport.setLatitude(lat);
        fireReport.setLongitude(lon);
        fireReport.setGeoipLocation(lat + "," + lon);
        fireReport.setFilename(filename == null ? "" : filename);
        checkForAlarm(lat, lon);
        elasticFacade.create(fireReport);
    }

    public void subscribe(String email, String lat, String lon) {
        System.out.println("subscribe: email=" + email + " lat=" + lat + " lon=" + lon);
        emails.put(email, new LatLon(lat, lon));
    }

    private void checkForAlarm(String lat, String lon) {
        emails.entrySet().stream()
                .filter(e -> isNear(e.getValue(), lat, lon))
                .forEach(e -> {
                    LatLon latLon = e.getValue();
                    alarmMailService.sendMail(e.getKey(), latLon.getLatitude(), latLon.getLongitude(), distance(Double.valueOf(latLon.getLatitude()),
                            Double.valueOf(latLon.getLongitude()),
                            Double.valueOf(lat),
                            Double.valueOf(lon)));
                });
    }

    private boolean isNear(LatLon latLon, String lat, String lon) {
        return distance(Double.valueOf(latLon.getLatitude()),
                Double.valueOf(latLon.getLongitude()),
                Double.valueOf(lat),
                Double.valueOf(lon)) < 5;
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return dist * 1.609344;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public String saveImage(InputStream inputStream, String mimeType) {
        String extension = mimeType.replaceAll("image/", "");
        String filename = "/tmp/nasa/" + System.currentTimeMillis() + "." + extension;
        try {
            FileUtils.copyInputStreamToFile(inputStream, new File(filename));
        } catch (Exception ignored) {
        }
        return filename;
    }

}
