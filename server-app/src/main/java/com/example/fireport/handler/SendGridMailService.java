package com.example.fireport.handler;

import com.sendgrid.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

@Component
@RequiredArgsConstructor
public class SendGridMailService {

    private final SendGrid sendGrid;

    public void sendTextMail(String fromEmail, String fromName, String toEmail, String subject, String content) {
        sendMail(fromEmail, fromName, singletonList(toEmail), emptyList(), subject, content, "text/plain");
    }

    private void sendMail(String fromEmail, String fromName, List<String> toEmails, List<String> ccEmails, String subject, String content, String contentType) {
        Mail mail = new Mail();
        mail.setFrom(new Email(fromEmail, fromName));
        mail.setSubject(subject);
        mail.addContent(new Content(contentType, content));
        Personalization personalization = new Personalization();
        for (String toEmail : toEmails) {
            personalization.addTo(new Email(toEmail));
        }
        for (String ccEmail : ccEmails) {
            personalization.addCc(new Email(ccEmail));
        }
        mail.addPersonalization(personalization);
        try {
            sendMail(mail.build());
            System.out.println("SendGrid. Sent email to=" + toEmails + " from=" + fromEmail + " subject=" + subject);
        } catch (IOException ex) {
            System.out.println("SendGrid. Failed to send email to=" + toEmails + " from=" + fromEmail + " subject=" + subject);
        }
    }

    private void sendMail(String body) throws IOException {
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(body);
        sendGrid.api(request);
    }

}
