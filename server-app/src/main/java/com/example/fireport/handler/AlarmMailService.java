package com.example.fireport.handler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@Component
@RequiredArgsConstructor
public class AlarmMailService {

    private static final String FROM_EMAIL = "fireport@fireport.com";
    private static final String FROM_NAME = "Fireport";

    private final SendGridMailService sendGridMailService;

    private String content = "Hi\n" +
            "\n" +
            "We received notification about fire near you.\n" +
            "\n" +
            "Location: <lat>,<lon>\n" +
            "\n" +
            "Distance: <distance> km\n" +
            "\n" +
            "Verify if it possible, and please be careful.";

    public void sendMail(String email, String lat, String lon, double distance) {
        System.out.println("Sending alarm email to " + email);
        String subject = "Fireport Alarm: fire near you";
        String content = getContent(lat, lon, distance);
        sendGridMailService.sendTextMail(FROM_EMAIL, FROM_NAME, email, subject, content);
    }

    private String getContent(String lat, String lon, double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        return content.replaceAll("<lat>", lat)
                .replaceAll("<lon>", lon)
                .replaceAll("<distance>", formatter.format(distance));
    }

}
