package com.example.fireport.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LatLon {

    private String latitude;
    private String longitude;

}
