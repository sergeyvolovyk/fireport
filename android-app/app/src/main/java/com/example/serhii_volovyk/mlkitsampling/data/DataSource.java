package com.example.serhii_volovyk.mlkitsampling.data;

import android.support.annotation.NonNull;

import com.example.serhii_volovyk.mlkitsampling.data.dto.ReportDTO;
import com.example.serhii_volovyk.mlkitsampling.data.dto.SubscriptionDTO;

public interface DataSource {

    interface SendReportCallback {

        void onReportSend();

        void onReportFailure();
    }

    interface SubscriptionCallback {

        void onSubscriptionSuccess();

        void onSubscriptionFailure();
    }

    void sendReport(@NonNull ReportDTO report, @NonNull SendReportCallback callback);

    void subscribeToFireport(@NonNull SubscriptionDTO subscription,
                             @NonNull SubscriptionCallback callback);
}
