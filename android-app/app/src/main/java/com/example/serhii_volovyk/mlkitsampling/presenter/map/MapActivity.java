package com.example.serhii_volovyk.mlkitsampling.presenter.map;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.example.serhii_volovyk.mlkitsampling.R;
import com.example.serhii_volovyk.mlkitsampling.presenter.subscribe.SubscribeActivity;

import java.util.Objects;

public class MapActivity extends AppCompatActivity {

    Button subscribeToEmailButton;

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        subscribeToEmailButton = findViewById(R.id.subscribeToEmailButton);

        webView = findViewById(R.id.webview);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        String url = "http://192.168.31.18:5601/app/kibana#/visualize/edit/9bedff30-d478-11e8-b494-55504827fc77?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-90d%2Cmode%3Aquick%2Cto%3Anow))";

        webView.loadUrl(url);

        subscribeToEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MapActivity.this, SubscribeActivity.class);
                MapActivity.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
