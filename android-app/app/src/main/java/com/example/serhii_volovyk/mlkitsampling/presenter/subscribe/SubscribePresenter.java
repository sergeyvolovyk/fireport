package com.example.serhii_volovyk.mlkitsampling.presenter.subscribe;

import android.support.annotation.NonNull;

import com.example.serhii_volovyk.mlkitsampling.data.DataSource;
import com.example.serhii_volovyk.mlkitsampling.data.Repository;
import com.example.serhii_volovyk.mlkitsampling.data.dto.SubscriptionDTO;

public class SubscribePresenter implements SubscribeContract.Presenter {

    private String TAG = SubscribePresenter.class.getSimpleName();

    private SubscribeContract.View subscribeView;

    private Repository repository;

    public SubscribePresenter(@NonNull SubscribeContract.View subscribeView) {
        this.subscribeView = subscribeView;
        this.subscribeView.setPresenter(this);
        repository = new Repository();
    }

    @Override
    public void subscribe(final SubscriptionDTO subscription) {
        subscribeView.startSpinner();
        repository.subscribeToFireport(subscription, new DataSource.SubscriptionCallback() {
            @Override
            public void onSubscriptionSuccess() {
                subscribeView.onSubscriptionSuccessful();
                subscribeView.stopSpinner();
            }

            @Override
            public void onSubscriptionFailure() {
                subscribeView.onSubscriptionFailure();
                subscribeView.stopSpinner();
            }
        });
    }

    @Override
    public void start() {

    }
}
