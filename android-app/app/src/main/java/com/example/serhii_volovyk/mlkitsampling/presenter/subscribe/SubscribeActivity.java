package com.example.serhii_volovyk.mlkitsampling.presenter.subscribe;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.serhii_volovyk.mlkitsampling.R;
import com.example.serhii_volovyk.mlkitsampling.data.dto.SubscriptionDTO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Objects;

public class SubscribeActivity extends AppCompatActivity implements SubscribeContract.View {

    SubscribeContract.Presenter presenter;

    EditText emailEditText;

    EditText latitudeEditText;

    EditText longitudeEditText;

    Button subscribeButton;

    ProgressBar progressBar;

    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        presenter = new SubscribePresenter(this);

        emailEditText = findViewById(R.id.emailEditText);
        latitudeEditText = findViewById((R.id.latEditText));
        longitudeEditText = findViewById(R.id.longEditText);
        subscribeButton = findViewById(R.id.subscribeToFireportButton);
        progressBar = findViewById(R.id.subscribeProgressBar);

        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString();
                float latitude = Float.valueOf(latitudeEditText.getText().toString());
                float longitude = Float.valueOf(longitudeEditText.getText().toString());
                SubscriptionDTO subscription = new SubscriptionDTO(email, latitude, longitude);
                presenter.subscribe(subscription);
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Location permission problem!", Toast.LENGTH_LONG).show();
            return;
        } else {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                latitudeEditText.setText(String.valueOf(location.getLatitude()));
                                longitudeEditText.setText(String.valueOf(location.getLongitude()));
                            }
                        }
                    });
        }
    }

    @Override
    public void onSubscriptionSuccessful() {
        Toast.makeText(this, "You have subscribed!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSubscriptionFailure() {
        Toast.makeText(this, "Subscription failure!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void startSpinner() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopSpinner() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setPresenter(SubscribeContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
