package com.example.serhii_volovyk.mlkitsampling.data.dto;

import com.google.gson.annotations.SerializedName;

public class SubscriptionDTO {

    @SerializedName("email")
    private String email;

    @SerializedName("latitude")
    private float latitude;

    @SerializedName("longitude")
    private float longitude;

    public SubscriptionDTO(String email, float latitude, float longitude) {
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getEmail() {
        return email;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }
}
