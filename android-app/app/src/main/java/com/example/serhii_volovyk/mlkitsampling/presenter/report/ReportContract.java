package com.example.serhii_volovyk.mlkitsampling.presenter.report;

import com.example.serhii_volovyk.mlkitsampling.data.dto.ReportDTO;
import com.example.serhii_volovyk.mlkitsampling.presenter.BasePresenter;
import com.example.serhii_volovyk.mlkitsampling.presenter.BaseView;

public interface ReportContract {
    interface View extends BaseView<Presenter> {

        void onSendingSuccessful();

        void onSendingFailure();

        void startSpinner();

        void stopSpinner();
    }

    interface Presenter extends BasePresenter {
        void send(ReportDTO report);
    }
}
