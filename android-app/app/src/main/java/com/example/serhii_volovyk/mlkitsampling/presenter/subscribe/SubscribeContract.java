package com.example.serhii_volovyk.mlkitsampling.presenter.subscribe;

import com.example.serhii_volovyk.mlkitsampling.data.dto.SubscriptionDTO;
import com.example.serhii_volovyk.mlkitsampling.presenter.BasePresenter;
import com.example.serhii_volovyk.mlkitsampling.presenter.BaseView;

public interface SubscribeContract {
    interface View extends BaseView<Presenter> {

        void onSubscriptionSuccessful();

        void onSubscriptionFailure();

        void startSpinner();

        void stopSpinner();
    }

    interface Presenter extends BasePresenter {
        void subscribe(SubscriptionDTO subscription);
    }
}
